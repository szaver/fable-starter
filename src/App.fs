﻿module App

open Fable.Core.JsInterop
open Fable.Import

let window = Browser.Dom.window

let mutable titleEl: Browser.Types.HTMLHeadingElement = unbox window.document.getElementById "title"

titleEl.innerHTML <- "Hello from Fable!"
